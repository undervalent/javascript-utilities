# README

This is just Some Useful Javascript utlITieS (sujits); sorry, I am not very creative, that have helpled me. All have been found on Stack Overflow.

## Why sujits?

Mainly because I found myself needing to write these utilities over and over again so I figured why not just package them.

## Installation

```
npm i --save sujits
```

## Usasge

### Browser Utilities

#### getQuerystring

```
sujits.getQuerystring('monkey');
```

- When run in the browswer will return the value in the query string of the url.

#### setBrowserClass

```
sujits.setBrowserClass();
```

- When run in the browswer will return: is-chrome, is-firefox, is-safari, is-ie, is-ie-edge, is-opera

#### isMobileOS

```
sujits.isMobileOS();
```

- detectMobileOS() => false //If on desktop;
- detectMobileOS() => true; //If OS is one of the provided agents in the list: 'Android',
  'webOS',
  'iPhone',
  'iPad',
  'iPod',
  'BlackBerry',
  'Windows Phone',

### Detect Sizes

You can pass in the an optional paramater of breakpoint size for each of these size detectors.

#### isMobile

```
sujits.isMobile();
```

- Will tell you if your value is in the mobile bounds.
- Default is set to 640 based on foundation grid breakpoints.

#### detectTabletSize

Can take two optional paramaters one for mobile breakpoint (default set to 640) and one for desktop breakpoint (default set to 1024).

```
sujits.detectTabletSize();
```

#### isTabletDown

Will tell you if your value is in or below tablet bounds. Default is set to 1024.

```
sujits.detectTabletDown();
```

- Will tell you if your value is in or below tablet bounds.

#### isDesktop

```
sujits.isDesktop();
```

- Will tell you if your value is in desktop bounds.
- Default is set to 1025 as this is tablet breakpoint.

### General help utilities

#### ordinalSuffix

```
sujits.ordinalSuffix(2); // 2nd
```

This will will take a number and return you the suffix such as 1st when you pass in a 1 and 2nd when you pass in a 2 etc.

#### closest

```
sujits.closest('.classname');
```

These resemble jquery's closest functions which comes in very handy when trying to traverse the dom in vanillaJS

#### delay

```
sujits.delay();
```

A delay function useful for search as you type functionality in that the user can type a few leters before it fires off.
