import setBrowserClass from './browserUtils/setBrowserClass';
import isMobileOS from './browserUtils/detectMobileOS';
import isDesktop from './browserUtils/detectSizes/detectDesktopSize';
import isMobile from './browserUtils/detectSizes/detectMobileSize';
import isTabletDown from './browserUtils/detectSizes/detectTabletDown';
import isTablet from './browserUtils/detectSizes/detectTabletSize';
import closest from './utilities/closest';
import delay from './utilities/delay';
import getQueryString from './utilities/getQueryString';
import ordinalSuffix from './utilities/ordinalSuffix';
import awaitAllPromises from './utilities/awaitAllPromises';
import nFormatter from './utilities/numberFormatter';

module.exports = {
  setBrowserClass,
  isMobileOS,
  isMobile,
  isTabletDown,
  isTablet,
  isDesktop,
  closest,
  delay,
  getQueryString,
  ordinalSuffix,
  awaitAllPromises,
  nFormatter
};
