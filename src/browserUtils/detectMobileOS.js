/**
 * Determins if you are on a mobile OS or not.
 * Checks the navigator.userAgent string
 * @returns {boolean}
 * @example
 * * detectMobileOS() => false //If on desktop;
 * * detectMobileOS() => true; //If on one of the provided agents in the list
 */

function detectMobileOS() {
  const mobileOsList = [
    'Android',
    'webOS',
    'iPhone',
    'iPad',
    'iPod',
    'BlackBerry',
    'Windows Phone',
  ];
  const list = new RegExp(mobileOsList.join('|'), 'i');
  return list.test(navigator.userAgent);
}

export default detectMobileOS;
