/**
 * Will tell you if your value is in or below tablet bounds.
 * Default is set to 1024.
 *
 * @param {tabletBreakpoint} tabletBreakpoint
 * @returns {boolean}
 * @example
 * *detectTabletDown(1023) => true;
 * *detectTabletDown(1024) => true;
 * *detectTabletDown(1025) => false;
 * *detectTabletDown(1026) => false;
 */
function detectTabletDown(tabletBreakpoint) {
  return window.innerWidth <= tabletBreakpoint || 1024;
}

export default detectTabletDown;
