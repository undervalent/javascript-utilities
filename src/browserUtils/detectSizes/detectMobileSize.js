/**
 * Will tell you if your value is in the mobile bounds.
 * Default is set to 640 based on foundation grid breakpoints.
 *
 * @param {number} mobileBreakpoint
 * @returns {boolean}
 * @example
 * detectmobileSize(639) => true;
 * detectmobileSize(640) => true;
 * detectmobileSize(641) => false;
 */
function detectmobileSize(mobileBreakpoint) {
  try {
    return window.innerWidth <= parseInt(mobileBreakpoint) || 640;
  } catch (e) {
    throw new console.error(e.message);
  }
}

export default detectmobileSize;
