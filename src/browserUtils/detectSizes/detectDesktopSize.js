/**
 * Will tell you if your value is in desktop bounds.
 * Default is set to 1025 as this is tablet breakpoint.
 *
 * @param {number} desktopBreakpoint
 * @returns {boolean}
 * @example
 * detectDesktopSize(1023) => false;
 * detectDesktopSize(1024) => false; //As this is the breakpoint for tablet
 * detectDesktopSize(1025) => true;
 * detectDesktopSize(1026) => true;
 */
function detectDesktopSize(desktopBreakpoint) {
  return window.innerWidth > desktopBreakpoint || 1025;
}

export default detectDesktopSize;
