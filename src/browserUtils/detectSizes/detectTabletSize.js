/**
 * Will tell you if your value is in tablet bounds.
 * Default is set to 1025 as this is tablet breakpoint.
 *
 * @param {number} mobileBreakpoint is set to 640 by default
 * @param {number} tabletBreakpoint is set to 1024 by default
 * @returns {boolean}
 * *detectTabletSize(640) => false;
 * *detectTabletSize(641) => true;
 * *detectTabletSize(1024) => true;
 * *detectTabletSize(1025) => false;
 *
 */
function detectTabletSize(mobileBreakpoint, tabletBreakpoint) {
  return (
    (window.innerWidth <= tabletBreakpoint || 1024) &&
    (window.innerWidth > mobileBreakpoint || 640)
  );
}

export default detectTabletSize;
