//This will will take a number and return you the suffix such as 1st when you pass in a 1 and 2nd when you pass in a 2.
const ordinalSuffixOf = i => {
  let j = i % 10,
    k = i % 100;
  if (j == 1 && k != 11) return i + 'st';
  if (j == 2 && k != 12) return i + 'nd';
  if (j == 3 && k != 13) return i + 'rd';
  return i + 'th';
};

export default ordinalSuffixOf;
