/**
 * Allows you to await a series of promises.
 * @param {Array,Function} list,functToAwait
 * @returns {Array}
 * @example
 * getAllPromises([1,2,3], (data)=>{
 *  Function call to API ex. data += 1;
 *
 * }) => [2,3,4] returns promised information;
 */

async function getAllPromises(list, funcToAwait) {
  try {
    const promises = list.map(async itemList => {
      const response = await funcToAwait(itemList);
      return response;
    });
    const allPromises = await Promise.all(promises);
    return allPromises;
  } catch (e) {
    console.log(e);
  }
}

exports.getAllPromises = getAllPromises;
