function getQuerystring(key) {
  key = key.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + key + '=([^&#]*)');
  return regex.exec(window.location.href) == null ? '' : qs[1];
}

export default getQuerystring;
