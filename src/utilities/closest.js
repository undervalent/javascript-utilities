/*
* These resemble jquery's closest functions which comes in very handy when trying to traverse the dom in vanillaJS
*/
export const closest = (el, fn) =>
  el && (fn(el) ? el : closest(el.parentNode, fn));
export const closestByClass = (el, className) =>
  closest(el, el => (el.className.indexOf(className) !== -1 ? el : null));

export default closestByClass;
