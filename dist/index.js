"use strict";

var _setBrowserClass = _interopRequireDefault(require("./browserUtils/setBrowserClass"));

var _detectMobileOS = _interopRequireDefault(require("./browserUtils/detectMobileOS"));

var _detectDesktopSize = _interopRequireDefault(require("./browserUtils/detectSizes/detectDesktopSize"));

var _detectMobileSize = _interopRequireDefault(require("./browserUtils/detectSizes/detectMobileSize"));

var _detectTabletDown = _interopRequireDefault(require("./browserUtils/detectSizes/detectTabletDown"));

var _detectTabletSize = _interopRequireDefault(require("./browserUtils/detectSizes/detectTabletSize"));

var _closest = _interopRequireDefault(require("./utilities/closest"));

var _delay = _interopRequireDefault(require("./utilities/delay"));

var _getQueryString = _interopRequireDefault(require("./utilities/getQueryString"));

var _ordinalSuffix = _interopRequireDefault(require("./utilities/ordinalSuffix"));

var _awaitAllPromises = _interopRequireDefault(require("./utilities/awaitAllPromises"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  setBrowserClass: _setBrowserClass.default,
  isMobileOS: _detectMobileOS.default,
  isMobile: _detectMobileSize.default,
  isTabletDown: _detectTabletDown.default,
  isTablet: _detectTabletSize.default,
  isDesktop: _detectDesktopSize.default,
  closest: _closest.default,
  delay: _delay.default,
  getQueryString: _getQueryString.default,
  ordinalSuffix: _ordinalSuffix.default,
  awaitAllPromises: _awaitAllPromises.default
};