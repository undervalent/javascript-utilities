"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/**
 * Allows you to await a series of promises.
 * @param {Array,Function} list,functToAwait
 * @returns {Array}
 * @example
 * getAllPromises([1,2,3], (data)=>{
 *  Function call to API ex. data += 1;
 *
 * }) => [2,3,4] returns promised information;
 */
function getAllPromises(_x, _x2) {
  return _getAllPromises.apply(this, arguments);
}

function _getAllPromises() {
  _getAllPromises = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(list, funcToAwait) {
    var promises, allPromises;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            promises = list.map(
            /*#__PURE__*/
            function () {
              var _ref = _asyncToGenerator(
              /*#__PURE__*/
              regeneratorRuntime.mark(function _callee(itemList) {
                var response;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return funcToAwait(itemList);

                      case 2:
                        response = _context.sent;
                        return _context.abrupt("return", response);

                      case 4:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));

              return function (_x3) {
                return _ref.apply(this, arguments);
              };
            }());
            _context2.next = 4;
            return Promise.all(promises);

          case 4:
            allPromises = _context2.sent;
            return _context2.abrupt("return", allPromises);

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this, [[0, 8]]);
  }));
  return _getAllPromises.apply(this, arguments);
}

exports.getAllPromises = getAllPromises;