"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function getQuerystring(key) {
  key = key.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + key + '=([^&#]*)');
  return regex.exec(window.location.href) == null ? '' : qs[1];
}

var _default = getQuerystring;
exports.default = _default;