"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.closestByClass = exports.closest = void 0;

/*
* These resemble jquery's closest functions which comes in very handy when trying to traverse the dom in vanillaJS
*/
var closest = function closest(el, fn) {
  return el && (fn(el) ? el : closest(el.parentNode, fn));
};

exports.closest = closest;

var closestByClass = function closestByClass(el, className) {
  return closest(el, function (el) {
    return el.className.indexOf(className) !== -1 ? el : null;
  });
};

exports.closestByClass = closestByClass;
var _default = closestByClass;
exports.default = _default;