"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function setBrowserClass() {
  // Opera 8.0+
  var isOpera = !!window.opr && !!opr.addons || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; // Firefox 1.0+

  var isFirefox = typeof InstallTrigger !== 'undefined'; // At least Safari 3+: "[object HTMLElementConstructor]"

  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0; // Internet Explorer 6-11

  var isIE =
  /* @cc_on!@ */
  !!document.documentMode; // Edge 20+

  var isEdge = !isIE && !!window.StyleMedia; // Chrome 1+

  var isChrome = !!window.chrome && !!window.chrome.webstore;
  if (isFirefox) return 'is-firefox';
  if (isSafari) return 'is-safari';
  if (isChrome) return 'is-chrome';
  if (isIE) return 'is-ie';
  if (isEdge) return 'is-ie-edge';
  if (isOpera) return 'is-opera';
}

var _default = setBrowserClass;
exports.default = _default;