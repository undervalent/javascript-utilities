"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * Will tell you if your value is in or below tablet bounds.
 * Default is set to 1024.
 *
 * @param {tabletBreakpoint} tabletBreakpoint
 * @returns {boolean}
 * @example
 * *detectTabletDown(1023) => true;
 * *detectTabletDown(1024) => true;
 * *detectTabletDown(1025) => false;
 * *detectTabletDown(1026) => false;
 */
function detectTabletDown(tabletBreakpoint) {
  return window.innerWidth <= tabletBreakpoint || 1024;
}

var _default = detectTabletDown;
exports.default = _default;