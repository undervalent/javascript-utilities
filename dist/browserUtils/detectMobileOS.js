"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * Determins if you are on a mobile OS or not.
 * Checks the navigator.userAgent string
 * @returns {boolean}
 * @example
 * * detectMobileOS() => false //If on desktop;
 * * detectMobileOS() => true; //If on one of the provided agents in the list
 */
function detectMobileOS() {
  var mobileOsList = ['Android', 'webOS', 'iPhone', 'iPad', 'iPod', 'BlackBerry', 'Windows Phone'];
  var list = new RegExp(mobileOsList.join('|'), 'i');
  return list.test(navigator.userAgent);
}

var _default = detectMobileOS;
exports.default = _default;